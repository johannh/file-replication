package com.johann_hofmann.daa1;

import org.jfree.chart.ChartPanel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(final String[] args) {

        // exit if method and n are not provided
        if (args.length < 2) {
            System.err.println(
                    "Usage: <iterative|memoized|recursive> <number of servers> [-g|-r|-s]\n" +
                            "Arguments:\n" +
                            "-r: specify a range for randomizing the placement costs, needs two arguments\n" +
                            "-s: measure and print stats\n" +
                            "-g: show generated graphs"
            );
            System.exit(1);
        }

        // get position of optional arguments
        int argumentIndex = 0;
        for (int i = 0; i < args.length; i++) {
            argumentIndex = i;
            try {
                Integer.parseInt(args[i]);
                break;
            } catch (final NumberFormatException ignored) {
            }
        }

        // get n from arguments
        final int n = Integer.parseInt(args[argumentIndex++]);

        // optional arguments

        // -r
        int min = 5;
        int max = 5;

        // -s
        boolean statistics = false;

        // -g
        boolean graphs = false;

        // parse arguments
        for (int i = argumentIndex; i < args.length; i++) {
            switch (args[i]) {
                case "-r": // specify a range for randomizing the placement costs
                    min = Integer.parseInt(args[++i]);
                    max = Integer.parseInt(args[++i]);
                    break;
                case "-s": // measure and print statistics
                    statistics = true;
                    break;
                case "-g": // show generated graphs
                    graphs = true;
                    break;
                default:
                    System.out.println("Warning: Unknown argument " + args[i]);
            }
        }

        // generate random placement costs for each node
        final int[] placement_costs = Randomizer.generateCosts(n, min, max);

        // save stathelpers for later evaluation
        final List<StatHelper> statHelpers = new ArrayList<>();

        // used to simultaneously run multiple algorithms at once
        final ExecutorService es = Executors.newCachedThreadPool();

        for (int i = 0; i < argumentIndex - 1; i++) {
            final String method = args[i];

            StatHelper statHelper = null;
            if (statistics) {
                statHelper = new StatHelper(method);
                statHelpers.add(statHelper);
            }

            final FileAlgorithm algorithm = new FileAlgorithm(method, placement_costs, statHelper);

            // parallel execution
            es.execute(algorithm);
        }
        es.shutdown();

        try {
            // all tasks have finished or the time has been reached.
            es.awaitTermination(30, TimeUnit.SECONDS);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }

        if (statistics) {
            final Grapher stepGrapher = new Grapher("Calculation steps", "n", "steps");
            final Grapher timeGrapher = new Grapher("Time", "n", "nanoseconds");

            stepGrapher.addReferenceValues(n);

            // add stathelper stats to graph
            for (final StatHelper statHelper : statHelpers) {
                statHelper.printServers();
                if (graphs) {
                    stepGrapher.addSteps(statHelper.getMethod(), statHelper.getStats());
                    timeGrapher.addTime(statHelper.getMethod(), statHelper.getStats());
                }
            }

            if (graphs) {
                // create new window with graph
                final JFrame frame = new JFrame();
                final ChartPanel chartPanel = new ChartPanel(stepGrapher.createChart());
                chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
                frame.setContentPane(chartPanel);
                frame.pack();
                frame.setVisible(true);

                // create new window with graph
                final JFrame timeFrame = new JFrame();
                final ChartPanel timeChartPanel = new ChartPanel(timeGrapher.createChart());
                timeChartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
                timeFrame.setContentPane(timeChartPanel);
                timeFrame.pack();
                timeFrame.setVisible(true);
            }
        }
    }
}
