package com.johann_hofmann.daa1;

/**
 * Randomization tools for the {@link com.johann_hofmann.daa1.FileAlgorithm}
 */
public class Randomizer {

    /**
     * Generate random access costs for n nodes
     *
     * @param n number of nodes
     * @param min the minimum value
     * @param max the maximum value
     * @return an array sized n + 1 with random access costs
     */
    public static int[] generateCosts(final int n, final int min, final int max) {

        final int[] costs = new int[n + 1];

        for (int i = 0; i <= n; i++) {
            costs[i] = (int) (Math.random() * (max - min)) + min;
        }
        return costs;
    }

}
